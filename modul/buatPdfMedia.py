from telegram import Bot, Update
from config import *
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph, Spacer, Image, Preformatted
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import A4
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT
from reportlab.lib import colors, utils
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from config import *
from datetime import datetime
import html
from bidi.algorithm import get_display
from arabic_reshaper import ArabicReshaper
# from emojipy import Emoji
import emoji
from emojipy import Emoji
import re
import os


Emoji.unicode_alt = False


def replace_with_emoji_pdf(text, size):
    """
    Reportlab's Paragraph doesn't accept normal html <image> tag's attributes
    like 'class', 'alt'. Its a little hack to remove those attrbs
    """

    text = Emoji.to_image(text)
    text = text.replace('class="emojione"', 'height=%s width=%s' %
                        (size, size))
    return re.sub('alt="'+Emoji.shortcode_regexp+'"', '', text)

configuration = {
    'delete_harakat': False,
    'support_ligatures': False,    
}
reshaper = ArabicReshaper(configuration=configuration)


def buatPdf(bot:Bot,update:Update):
    message         = update.effective_message  # type: Optional[Message]            
    chat_id         = message.chat.id
    namafile    = "%s.pdf"%(abs(chat_id))
    width, height = A4
    pdfmetrics.registerFont(TTFont('Candara','arial_0.ttf'))
    pdfmetrics.registerFont(TTFont('CandaraBold','arialbd_0.ttf'))
    pdfmetrics.registerFont(TTFont('CandaraItalic','ariali_0.ttf'))

    
    doc = SimpleDocTemplate(namafile, rightMargin=.5 * cm, leftMargin=.5 * cm,topMargin=.5 * cm, bottomMargin=1.5 * cm)
    story = []        
    styles = getSampleStyleSheet()
    styles.add(ParagraphStyle(name='Center', alignment=TA_CENTER))
    styles.add(ParagraphStyle(name='Right', alignment=TA_RIGHT))
    styles.add(ParagraphStyle(name='Left', alignment=TA_LEFT))
    styles.add(ParagraphStyle(name='Line_Data', fontName='Candara',alignment=TA_LEFT, fontSize=10, leading=7))
    # styles.add(ParagraphStyle(name='Line_Data_Angka', fontName='Candara',alignment=TA_RIGHT, fontSize=8, leading=7))
    # styles.add(ParagraphStyle(name='Line_Data_Small', fontName='Candara',alignment=TA_LEFT, fontSize=7, leading=8))
    styles.add(ParagraphStyle(name='Line_Data_Large', fontName='CandaraBold',alignment=TA_LEFT, fontSize=11, leading=12))
    # styles.add(ParagraphStyle(name='Line_Data_Largest', fontName='Candara',alignment=TA_LEFT, fontSize=15, leading=15))
    styles.add(ParagraphStyle(name='Line_Label', fontName='Candara', fontSize=11, leading=13, alignment=TA_LEFT, wordWrap = True))
    # styles.add(ParagraphStyle(name='Line_Label_Center', fontName='Candara', fontSize=10, alignment=TA_CENTER))
    styles.add(ParagraphStyle(name='judul', fontName='CandaraBold', fontSize=16, alignment=TA_CENTER,leading = 20))
    styles.add(ParagraphStyle(name='username', fontName='CandaraBold', fontSize=11, alignment=TA_LEFT,leading = 7))
    styles.add(ParagraphStyle(name='reply', fontName='CandaraItalic', fontSize=10, alignment=TA_LEFT,leading = 10))    

    story.append(Paragraph("<b>Export MEdia</b>", styles["judul"]))
    story.append(Spacer(0.1 * cm, .8 * cm))

    sql_rangkum = "SELECT media_keyword,media_tipe,media_id FROM media WHERE chat_id = '%s'"%(chat_id)
    barR,jumR = eksekusi(sql_rangkum)

    for i in range(jumR):
        media_keyword = str( barR[i][0])
        media_tipe    = str(barR[i][1])
        media_id      = str(barR[i][2])

        if os.path.exists('gambar/%s'%media_id)==False:
            sticker     = message.sticker        
            photo       = message.photo
            video       = message.animation
            if media_tipe == 'photo':
                media       = photo[-1].file_id
                isi         = message.caption
                teks_media  = str(media)
                media_file  = bot.get_file(media)
                media_file.download('gambar/%s'%media)
                width       = photo[-1].width
                height      = photo[-1].height
                image_size  = "%sx%s"%(width,height)
            elif media_tipe == 'sticker':            
                media       = sticker['thumb']['file_id']
                teks_media  = str(media)
                isi         = ""
                media_file  = bot.get_file(media)
                media_file.download('gambar/%s'%media)
                width       = sticker['thumb']['width']
                height      = sticker['thumb']['height']
                image_size  = "%sx%s"%(width,height)
            elif media_tipe == 'video' or 'document':
                media       = video['thumb']['file_id']
                teks_media  = str(media)
                isi         = ""
                media_file  = bot.get_file(media)
                media_file.download('gambar/%s'%media)
                width       = video['thumb']['width']
                height      = video['thumb']['height']
                image_size  = "%sx%s"%(width,height)
            else:
                isi         = msg_text
                teks_media  = ""
                image_size  = "0x0"
        data1 = [[
            Paragraph('', styles["Line_Data"]),
            Paragraph('%s'%media_keyword, styles["Line_Data"]),
            Paragraph('<img src = "gambar/%s" width = 32 height = 32 valign = "top"></img>'%media_id, styles["Line_Data"]),
            ]]
        t1 = Table(data1, colWidths=dataLISTwidth)            
        t1.hAlign = 'LEFT'
        t1.setStyle(TableStyle([
            # ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
            # ('BOX', (0, 0), (-1, -1), 0.25, colors.black),
            ('VALIGN', (0, 0), (-1, -1), 'TOP'),
        ]))
        story.append(t1)

        story.append(Spacer(0.1 * cm, 1 * cm))

    doc.build(story)
# buatPdf(Bot,Update)