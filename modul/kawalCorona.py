#credit : https://github.com/Billal06/KawalCorona

import requests
import time
from telegram import Bot, Update
from config import *
from telegram import ParseMode
from telegram import MessageEntity
import threading
import random

# lock = threading.Lock()

def cor(bot:Bot,update:Update):
    data1 = []
    data2 = []
    tampil = []
    negara = "indonesia"
    url_utama = "https://api.kawalcorona.com/"
    url_prov = "https://api.kawalcorona.com/indonesia/provinsi"

    m = update.effective_message            
    teks =  m.text.split(None,1)
    if len(teks)==1:
        update.message.reply_text("Perintahnya kurang banyak")        
    elif len(teks) == 2:
        a_split = teks[1].split(" ")
        if a_split[0].lower() == "sub":
            provinsi = (' '.join(a_split[1:]))
            tampil.append("Jumlah pasien korona area %s akan di laporkan berkala"%provinsi)
        else:
            provinsi = (teks[1])

        # try:
        # lock.acquire(True)
        # chat_id = str(update.message["chat"]["id"])
        # chat_type = update.message["chat"]["type"]
        # user_id = str(update.message.from_user.id)
        # user_name = update.message.from_user.username
        req=requests.get(url_utama).json()
        for x in req:
            data1.append(x['attributes'])
        for a in data1:
            country = a["Country_Region"]
            if negara.lower() in country.lower():
                tampil.append("Last Update "+time.ctime(int(str(a["Last_Update"])[:-3])))

        
        list_prov = []
        req=requests.get(url_prov).json()
        for x in req:
            data2.append(x['attributes'])

        for a in data2:
            list_prov.append(a['Provinsi'].lower())
        # try:
        list_prov.index(provinsi)
        for a in data2:
            prov = a["Provinsi"]        
            if provinsi.lower() in prov.lower():
                tampil.append(" Provinsi\t: %s\n Positif\t\t: %s\n Sembuh\t\t\t: %s\n Meninggal: %s"%(prov,str(a['Kasus_Posi']),str(a["Kasus_Semb"]),str(a['Kasus_Meni'])))
        # except:
        #     tampil.append("Provinsi %s tidak ketemu"%provinsi)
        update.message.reply_text(''.join('%s \n'%tampil[i] for i in range(len(tampil))),parse_mode=ParseMode.MARKDOWN)
        # except Exception as e:
        #     update.message.reply_text("Error!\n%s"%str(e))
