# credit : https://github.com/diverglovsky/coronaindo/blob/master/coronaindo.py

import requests,re
# import requests
import pprint
import json
import datetime
import time

# # url_kota= "https://api.kawalcorona.com/indonesia/provinsi"

# # reqlist = requests.get(url_kota).json()
# cari = "jawa barat".lower()
# for x in reqlist:
#     # print (x['attributes']['Provinsi'])
#     a = re.search(cari,x['attributes']['Provinsi'].lower())    
#     if a!=None:
#         print(x['attributes']['Provinsi']+"\nterdeteksi \t: "+str(x['attributes']['Kasus_Posi'])+"\nmati \t\t: "+str(x['attributes']['Kasus_Meni'])+"\nsembuh \t\t: "+str(x['attributes']['Kasus_Semb'])+"\nsakit \t\t: "+str(x['attributes']['Kasus_Posi']-x['attributes']['Kasus_Meni']-x['attributes']['Kasus_Semb']))


# r = requests.get("https://api.kawalcorona.com").text
# get_country = re.findall('"Country_Region":"(.*?)"', r)
# country = "Indonesia"
# data = '{"OBJECTID":(.*?),"Country_Region":"%s","Last_Update":(.*?),"Lat":(.*?),"Long_":(.*?),"Confirmed":(.*?),"Deaths":(.*?),"Recovered":(.*?),"Active":(.*?)}}'%(country)
# cari = re.search(data, r)
# print (" -> Success")
# #   print (cari.group())
# last = str(cari.group(2))[:-3]
# print ("[*] Country: "+country)
# print ("[*] Last Update: "+time.ctime(int(last)))

# data = []
# perintah = "jawa barat"
# req=requests.get('https://api.kawalcorona.com/indonesia/provinsi').json()
# for x in req:
#     data.append(x['attributes'])
# for a in data:
#     prov = a["Provinsi"]
#     if perintah.lower() in prov.lower():
#         print ("Provinsi\t: "+prov)
#         print ("Positif\t\t: "+str(a['Kasus_Posi']))
#         print ("Sembuh\t\t: "+str(a["Kasus_Semb"]))
#         print ("Meninggal\t: "+str(a['Kasus_Meni']))


data1 = []
data2 = []
tampil = []
negara = "indonesia"
url_utama = "https://api.kawalcorona.com/"
url_prov = "https://api.kawalcorona.com/indonesia/provinsi"


m = ["/cor","sub jawa barat"]
teks =  m[1].split(None,1)
if len(teks)==0:
    print ("Perintahnya kurang banyak")        
elif len(teks) == 2:
    a_split = teks[1].split(" ")
    if a_split[0].lower() == "sub":
        print ("subscribe")
        provinsi = (' '.join(a_split[1:]))
    else:
        provinsi = (teks[1])

    # try:
    
    req=requests.get(url_utama).json()
    for x in req:
        data1.append(x['attributes'])
    for a in data1:
        country = a["Country_Region"]
        if negara.lower() in country.lower():
            epoc = int(str(a["Last_Update"])[:-3])
            waktu = datetime.datetime.fromtimestamp(epoc).strftime('%Y-%m-%d %H:%M:%S')
            tampil.append("Last Update "+time.ctime(epoc))

    
    list_prov = []
    # req=requests.get(url_prov).json()
    req = [{"attributes":{"FID":11,"Kode_Provi":31,"Provinsi":"DKI Jakarta","Kasus_Posi":627,"Kasus_Semb":43,"Kasus_Meni":62}},{"attributes":{"FID":12,"Kode_Provi":32,"Provinsi":"Jawa Barat","Kasus_Posi":119,"Kasus_Semb":6,"Kasus_Meni":17}},{"attributes":{"FID":16,"Kode_Provi":36,"Provinsi":"Banten","Kasus_Posi":103,"Kasus_Semb":1,"Kasus_Meni":4}},{"attributes":{"FID":15,"Kode_Provi":35,"Provinsi":"Jawa Timur","Kasus_Posi":77,"Kasus_Semb":8,"Kasus_Meni":4}},{"attributes":{"FID":13,"Kode_Provi":33,"Provinsi":"Jawa Tengah","Kasus_Posi":55,"Kasus_Semb":0,"Kasus_Meni":7}},{"attributes":{"FID":27,"Kode_Provi":73,"Provinsi":"Sulawesi Selatan","Kasus_Posi":33,"Kasus_Semb":0,"Kasus_Meni":1}},{"attributes":{"FID":14,"Kode_Provi":34,"Provinsi":"Daerah Istimewa Yogyakarta","Kasus_Posi":22,"Kasus_Semb":1,"Kasus_Meni":2}},{"attributes":{"FID":23,"Kode_Provi":64,"Provinsi":"Kalimantan Timur","Kasus_Posi":17,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":17,"Kode_Provi":51,"Provinsi":"Bali","Kasus_Posi":9,"Kasus_Semb":0,"Kasus_Meni":2}},{"attributes":{"FID":2,"Kode_Provi":12,"Provinsi":"Sumatera Utara","Kasus_Posi":8,"Kasus_Semb":0,"Kasus_Meni":1}},{"attributes":{"FID":21,"Kode_Provi":62,"Provinsi":"Kalimantan Tengah","Kasus_Posi":7,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":33,"Kode_Provi":94,"Provinsi":"Papua","Kasus_Posi":7,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":3,"Kode_Provi":13,"Provinsi":"Sumatera Barat","Kasus_Posi":5,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":10,"Kode_Provi":21,"Provinsi":"Kepulauan Riau","Kasus_Posi":5,"Kasus_Semb":0,"Kasus_Meni":1}},{"attributes":{"FID":1,"Kode_Provi":11,"Provinsi":"Aceh","Kasus_Posi":4,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":9,"Kode_Provi":18,"Provinsi":"Lampung","Kasus_Posi":4,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":20,"Kode_Provi":61,"Provinsi":"Kalimantan Barat","Kasus_Posi":3,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":28,"Kode_Provi":74,"Provinsi":"Sulawesi Tenggara","Kasus_Posi":3,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":6,"Kode_Provi":16,"Provinsi":"Sumatera Selatan","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":1}},{"attributes":{"FID":18,"Kode_Provi":52,"Provinsi":"Nusa Tenggara Barat","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":24,"Kode_Provi":65,"Provinsi":"Kalimantan Utara","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":25,"Kode_Provi":71,"Provinsi":"Sulawesi Utara","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":26,"Kode_Provi":72,"Provinsi":"Sulawesi Tengah","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":34,"Kode_Provi":91,"Provinsi":"Papua Barat","Kasus_Posi":2,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":4,"Kode_Provi":14,"Provinsi":"Riau","Kasus_Posi":1,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":5,"Kode_Provi":15,"Provinsi":"Jambi","Kasus_Posi":1,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":22,"Kode_Provi":63,"Provinsi":"Kalimantan Selatan","Kasus_Posi":1,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":31,"Kode_Provi":81,"Provinsi":"Maluku","Kasus_Posi":1,"Kasus_Semb":0,"Kasus_Meni":0}},{"attributes":{"FID":32,"Kode_Provi":82,"Provinsi":"Maluku Utara","Kasus_Posi":1,"Kasus_Semb":0,"Kasus_Meni":0}}]
    for x in req:
        data2.append(x['attributes'])

    for a in data2:
        list_prov.append(a['Provinsi'].lower())
    # try:
    list_prov.index(provinsi)
    for a in data2:
        prov = a["Provinsi"]        
        if provinsi.lower() in prov.lower():
            tampil.append("Provinsi\t: %s\nPositif\t\t: %s\nSembuh\t\t: %s\nMeninggal\t: %s\n"%(prov,str(a['Kasus_Posi']),str(a["Kasus_Semb"]),str(a['Kasus_Meni'])))
    # except:
    #     tampil.append("Provinsi %s tidak ketemu"%provinsi)
    print(''.join('%s \n'%tampil[i] for i in range(len(tampil))))